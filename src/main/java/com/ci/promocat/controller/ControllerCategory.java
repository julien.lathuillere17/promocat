package com.ci.promocat.controller;

import java.util.List;
import com.ci.promocat.model.DTO.DTOCategory;
import com.ci.promocat.service.ServiceCategory;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerCategory.WS_CATEGORY)
public class ControllerCategory {

	public static final String WS_CATEGORY = "/category";
	private final ServiceCategory serviceCategory;

	@Autowired
	public ControllerCategory(ServiceCategory serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	@ApiOperation("Méthode qui liste les catégories")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<DTOCategory> listCategories(@RequestParam String token) {
		return serviceCategory.listCategories(token);
	}
	@ApiOperation("Méthode qui ajoute une catégory")
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public DTOCategory addCategory(@RequestParam String token, @RequestParam String label) {
		return serviceCategory.addCategory(token, label);
	}

	@ApiOperation("Méthode qui supprime une catégory")
	@RequestMapping(value = "/", method = RequestMethod.DELETE)
	public void deleteCategory(@RequestParam String token, @RequestParam Long id) {
		serviceCategory.deleteCategory(token, id);
	}
}
