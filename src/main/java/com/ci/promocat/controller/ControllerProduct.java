package com.ci.promocat.controller;

import java.util.List;
import com.ci.promocat.model.DTO.DTOCategory;
import com.ci.promocat.model.DTO.DTOProduct;
import com.ci.promocat.model.DTO.DTOPromotion;
import com.ci.promocat.service.ServiceCategory;
import com.ci.promocat.service.ServiceProduct;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerProduct.WS_PRODUCT)
public class ControllerProduct {

	public static final String WS_PRODUCT = "/product";
	private final ServiceProduct serviceProduct;

	@Autowired
	public ControllerProduct(ServiceProduct serviceProduct) {
		this.serviceProduct = serviceProduct;
	}

	@ApiOperation("Méthode qui donne la liste des produits")
	@RequestMapping(value = "/listProducts", method = RequestMethod.GET)
	public List<DTOProduct> listProducts() {
		return serviceProduct.listProducts();
	}

	@ApiOperation("Méthode qui ajoute un produit")
	@PutMapping(value = "/")
	@ResponseBody
	public void addProduct(@RequestBody DTOProduct dtoProduct,
		@RequestParam(required = false) String token) {
		serviceProduct.addProduct(token, dtoProduct);
	}

	@ApiOperation("Méthode qui modifie un produit")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void modifyProduct(@RequestBody DTOProduct dtoProduct,
		@RequestParam String token) {
		serviceProduct.modifyProduct(token, dtoProduct);
	}

	@ApiOperation("Méthode qui supprime un produit")
	@RequestMapping(value = "/", method = RequestMethod.DELETE)
	public void deleteProduct(@RequestParam Long productId,
		@RequestParam String token) {
		serviceProduct.deleteProduct(token, productId);
	}

	@ApiOperation("Méthode qui ajoute une promotion")
	@RequestMapping(value = "/addPromotion", method = RequestMethod.PUT)
	public DTOPromotion addPromotion(@RequestBody DTOPromotion dtoPromotion,
		@RequestParam Long productId,
		@RequestParam String token) {
		return serviceProduct.addPromotion(dtoPromotion, productId, token);
	}

	@ApiOperation("Méthode qui supprime une promotion")
	@RequestMapping(value = "/deletePromotion", method = RequestMethod.DELETE)
	public void deletePromotion(@RequestParam Long promotionId,
		@RequestParam String token) {
		serviceProduct.deletePromotion(promotionId, token);
	}
}
