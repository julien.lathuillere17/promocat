package com.ci.promocat.controller;

import com.ci.promocat.model.DTO.DTOToken;
import com.ci.promocat.model.DTO.DTOUser;
import com.ci.promocat.service.ServiceUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ControllerUser.WS_USER)
public class ControllerUser {

	public static final String WS_USER = "/user/";
	private final ServiceUser serviceUser;

	@Autowired
	public ControllerUser(ServiceUser serviceUser) {
		this.serviceUser = serviceUser;
	}

	@ApiOperation("Méthode qui crée un administrateur au tout premier démarrage de l'application")
	@RequestMapping(value = "/createFirstUser", method = RequestMethod.PUT)
	public DTOUser createFirstUser() {
		return serviceUser.createFirstUser();
	}

	@ApiOperation("Méthode qui authentifie un utilisateur")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public DTOToken login(@RequestParam String email, @RequestParam String password) {
		return serviceUser.login(email, password);
	}
}
