package com.ci.promocat.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

/**
 * Cette classe se charge d'écrire les logs
 *
 * @author lroyo
 */
public final class UtilsLogs {

	/**
	 * Constructeur
	 */
	private UtilsLogs() {
	}

	/**
	 * Cette méthode écrit les logs dans le fichier avec
	 * le niveau correspondant
	 *
	 */
	private static void writeLogsToFile(final Level level, final Logger logger, final Exception exception,
		final String action, final String... params) {
		final StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
		String method = StringUtils.EMPTY;
		if(stacks.length > 3) {
			method = stacks[3].getMethodName();
		}
		String paramsS = "";
		if(params != null && params.length > 0) {
			paramsS = Arrays.stream(params).filter(Objects::nonNull).map(String::toString).collect(Collectors.joining(", "));
		}
		StringBuilder sb = new StringBuilder("\"method=\"").append(
			method).append("\"; action=\"").append(action).append("\"; param=\"").append(
			paramsS).append("\"");
		if(level.equals(Level.DEBUG)) {
			logger.debug(sb.toString());
		} else if(level.equals(Level.INFO)) {
			logger.info(sb.toString());
		} else if(level.equals(Level.WARN)) {
			logger.warn(sb.toString());
		} else if(level.equals(Level.ERROR)) {
			logger.error(sb.toString());
			if(exception != null) {
				logger.error(exception.getMessage(), exception);
			}
		}
	}

	/**
	 * Cette méthode écrit un log dans le fichier en fonction de son niveau
	 *
	 */
	public static void writeLogs(final Level level, final Logger logger,
		final String action, final String... param) {
		UtilsLogs.writeLogsToFile(level, logger, null, action, param);
	}

	/**
	 * Cette méthode écrit une exception dans le fichier de log
	 *
	 */
	public static void writeErrorLog(final Exception exception, final Logger logger) {
		UtilsLogs.writeLogsToFile(Level.ERROR, logger, exception, "Exception", StringUtils.EMPTY);
	}
}

