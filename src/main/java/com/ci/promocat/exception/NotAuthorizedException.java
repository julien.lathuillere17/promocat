package com.ci.promocat.exception;

import org.springframework.http.HttpStatus;

public class NotAuthorizedException extends ExceptionApp {
	private static final long serialVersionUID = -3924831206270765671L;

	/**
	 * Constructeur
	 *
	 * @param message : le message d'erreur
	 */
	public NotAuthorizedException(String message, Object... params) {
		super(message, HttpStatus.FORBIDDEN, "not_authorized", params);
	}
}
