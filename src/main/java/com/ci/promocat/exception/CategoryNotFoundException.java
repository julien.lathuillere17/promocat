package com.ci.promocat.exception;

import org.springframework.http.HttpStatus;

public class CategoryNotFoundException extends ExceptionApp {
	private static final long serialVersionUID = -3924831206270765671L;

	/**
	 * Constructeur
	 *
	 * @param message : le message d'erreur
	 */
	public CategoryNotFoundException(String message, Object... params) {
		super(message, HttpStatus.NOT_FOUND, "category_not_found", params);
	}
}
