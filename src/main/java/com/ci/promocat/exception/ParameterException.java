package com.ci.promocat.exception;

import org.springframework.http.HttpStatus;

public class ParameterException extends ExceptionApp {
	private static final long serialVersionUID = -3924831206270765671L;

	/**
	 * Constructeur
	 *
	 * @param message : le message d'erreur
	 */
	public ParameterException(String message, Object... params) {
		super(message, HttpStatus.BAD_REQUEST, "parameter_empty_error", params);
	}
}
