package com.ci.promocat.exception;

import org.springframework.http.HttpStatus;

public class ExceptionApp extends RuntimeException {

	protected HttpStatus status;
	protected String errorCode;

	public ExceptionApp(String message, HttpStatus status, String errorCode, Object... messageParams) {
		super(messageParams != null && messageParams.length > 0 ? String.format(message, messageParams) : message);
		this.status = status;
		this.errorCode = errorCode;
	}

	public ExceptionApp(Throwable cause, String message, HttpStatus status, String errorCode, Object... messageParams) {
		super(messageParams != null && messageParams.length > 0 ? String.format(message, messageParams) : message, cause);
		this.status = status;
		this.errorCode = errorCode;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getErrorCode() {
		return errorCode;
	}
}
