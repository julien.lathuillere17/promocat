package com.ci.promocat.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends ExceptionApp {
	private static final long serialVersionUID = -3924831206270765671L;

	/**
	 * Constructeur
	 *
	 * @param message : le message d'erreur
	 */
	public ProductNotFoundException(String message, Object... params) {
		super(message, HttpStatus.NOT_FOUND, "product_not_found", params);
	}
}
