package com.ci.promocat.model.DAO;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "promotion")
public class Promotion implements Serializable {

	@Id
	@Column(name = "id_promo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "taux")
	private Float taux;
	@Column(name = "date_debut")
	private Instant startDate;
	@Column(name = "date_fin")
	private Instant endDate;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_produit", referencedColumnName = "id_produit")
	private Product product;
	public Promotion(Float taux, Instant startDate, Instant endDate, Product product) {
		this.taux = taux;
		this.startDate = startDate;
		this.endDate = endDate;
		this.product = product;
	}

	public Promotion() {
	}

	public long getId() {
		return id;
	}

	public Float getTaux() {
		return taux;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public Product getProduct() {
		return product;
	}
}
