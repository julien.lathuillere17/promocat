package com.ci.promocat.model.DAO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {

	@Id
	@Column(name = "id_produit")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "label")
	private String label;
	@Column(name = "description")
	private String description;
	@Column(name = "image")
	private String image;
	@Column(name = "price")
	private Float price;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_category", referencedColumnName = "id_category")
	private Category category;

	public Product(String label, String description, String image, Float price, Category category) {
		this.label = label;
		this.description = description;
		this.image = image;
		this.price = price;
		this.category = category;
	}
	public Product() {
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public long getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}
