package com.ci.promocat.model.DAO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import at.favre.lib.crypto.bcrypt.BCrypt;
import org.apache.commons.codec.digest.Sha2Crypt;

@Entity
@Table(name = "users")
public class User implements Serializable {

	@Id
	@Column(name = "id_user")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "prenom")
	private String prenom;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;

	public User() {
	}

	public User(String name, String prenom, String email, String password) {
		this.name = name;
		this.prenom = prenom;
		this.email = email;
		this.password = BCrypt.withDefaults().hashToString(12, password.toCharArray());
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
}
