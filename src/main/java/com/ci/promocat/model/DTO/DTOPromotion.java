package com.ci.promocat.model.DTO;

import java.io.Serializable;
import java.time.Instant;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.ci.promocat.config.InstantAdapter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DTOPromotion implements Serializable {

	@JsonProperty("id")
	private long id;
	@JsonProperty("taux")
	private Float taux;
	@XmlAttribute(name = "startDate")
	@XmlJavaTypeAdapter(InstantAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Instant startDate;
	@XmlAttribute(name = "endDate")
	@XmlJavaTypeAdapter(InstantAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Instant endDate;

	public DTOPromotion(long id, Float taux, Instant startDate, Instant endDate) {
		this.id = id;
		this.taux = taux;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	public DTOPromotion() {
	}

	public long getId() {
		return id;
	}

	public Float getTaux() {
		return taux;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}
}
