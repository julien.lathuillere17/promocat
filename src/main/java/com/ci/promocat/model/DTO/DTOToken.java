package com.ci.promocat.model.DTO;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DTOToken implements Serializable {

	@JsonProperty("token")
	private String token;

	public DTOToken(String token) {
		this.token = token;
	}
}
