package com.ci.promocat.model.DTO;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DTOCategory implements Serializable {

	@JsonProperty("id")
	private long id;
	@JsonProperty("label")
	private String label;

	public DTOCategory(long id, String name) {
		this.label = name;
		this.id = id;
	}

	public DTOCategory() {
	}

	public long getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}
}
