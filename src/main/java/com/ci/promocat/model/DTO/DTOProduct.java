package com.ci.promocat.model.DTO;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private long id;
	@JsonProperty("label")
	private String label;
	@JsonProperty("description")
	private String description;
	@JsonProperty("image")
	private String image;
	@JsonProperty("price")
	private Float price;
	@JsonProperty("category")
	private DTOCategory category;
	@JsonProperty("promotions")
	private List<DTOPromotion> promotions;
	public DTOProduct(long id, String label, String description, String image, Float price, DTOCategory category, List<DTOPromotion> promotions) {
		this.id = id;
		this.label = label;
		this.description = description;
		this.image = image;
		this.price = price;
		this.category = category;
		this.promotions = promotions;
	}

	public DTOProduct() {
	}

	public String getImage() {
		return image;
	}

	public long getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	public Float getPrice() {
		return price;
	}

	public DTOCategory getCategory() {
		return category;
	}

	public List<DTOPromotion> getPromotions() {
		return promotions;
	}
}
