package com.ci.promocat.model;

import java.time.Instant;

public class Authentification {

	Instant endDate;
	String token;

	public Authentification(Instant endDate, String token) {
		this.endDate = endDate;
		this.token = token;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public String getToken() {
		return token;
	}
}
