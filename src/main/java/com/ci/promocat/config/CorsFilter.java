package com.ci.promocat.config;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CorsFilter implements Filter {

	private final Map<String, String> headers = new HashMap<>();

	@Override
	public void init(FilterConfig filterConfig) {
		for(final String headerName : Collections.list(filterConfig.getInitParameterNames())) {
			headers.put(headerName, filterConfig.getInitParameter(headerName));
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {
		if((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
			doFilter((HttpServletRequest) request, (HttpServletResponse) response);
		}
		chain.doFilter(request, response);
	}

	private void doFilter(HttpServletRequest request, HttpServletResponse response) {
		String origin = request.getHeader("Origin");
		if(origin == null) {
			origin = request.getHeader("Host");
			if(origin == null) {
				return;
			}
		}
		for(final Map.Entry<String, String> header : headers.entrySet()) {
			response.addHeader(header.getKey(), header.getValue().replace("${Origin}", origin));
		}
	}

	@Override
	public void destroy() {
		// Nothing to do here
	}
}
