package com.ci.promocat.config;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

public class InstantAdapter extends TemporalAccessorXmlAdapter<Instant> {

	public InstantAdapter() {
		super(DateTimeFormatter.ISO_INSTANT, Instant::from);
	}
}

