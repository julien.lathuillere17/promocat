package com.ci.promocat.config;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Cette classe s'occupe de la configuration de la persistence des données en BDD
 *
 * @author lroyo
 */
@Configuration
@EnableJpaRepositories(basePackages = {
	"com.ci.promocat.repository"
}, entityManagerFactoryRef = "entityManager")
@EnableSchedulerLock(defaultLockAtMostFor = "30s")
public class ConfigSpringPersistence {

	@Bean(name = "dataSource")
	public DataSource buildDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		String jdbc = System.getenv("JDBC_URL");
		if(StringUtils.isEmpty(jdbc)) {
			jdbc = "jdbc:postgresql://localhost:5432/promocat?createDatabaseIfNotExist=true&serverTimezone=UTC" +
				"&useLegacyDatetimeCode=false";
		}
		System.err.println("CONNECT::"+jdbc);
		dataSource.setUrl(jdbc);
		dataSource.setUsername("compose-postgres");
		dataSource.setPassword("compose-postgres");
		return dataSource;
	}

	@Bean(name = "lockProvider")
	public LockProvider lockProvider(DataSource dataSource) {
		return new JdbcTemplateLockProvider(dataSource, "shedlock");
	}

	@Bean(name = "entityManager")
	public LocalContainerEntityManagerFactoryBean buildEntityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(buildDataSource());
		entityManagerFactory.setJpaProperties(instanciateProperties());
		return entityManagerFactory;
	}

	@Bean(name = "transactionManager")
	public PlatformTransactionManager getJpaTransactionManager(EntityManagerFactory emf) {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Properties instanciateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.show_sql", "false");
		properties.setProperty("hibernate.use_sql_comments", "true");
		properties.setProperty("hibernate.format_sql", "true");
		properties.setProperty("hibernate.use_sql_comments", "true");
		properties.setProperty("spring.datasource.initialization-mode", "always");
		properties.setProperty("spring.datasource.platform", "mysql");
		properties.setProperty("spring.jpa.hibernate.ddl-auto", "update");
		return properties;
	}
}
