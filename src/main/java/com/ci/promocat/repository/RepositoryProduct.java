package com.ci.promocat.repository;

import com.ci.promocat.model.DAO.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryProduct extends CrudRepository<Product, Long> {
}
