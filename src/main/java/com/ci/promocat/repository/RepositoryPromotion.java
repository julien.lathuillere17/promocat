package com.ci.promocat.repository;

import com.ci.promocat.model.DAO.Product;
import com.ci.promocat.model.DAO.Promotion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPromotion extends CrudRepository<Promotion, Long> {

	Promotion findByProduct(Product product);
}
