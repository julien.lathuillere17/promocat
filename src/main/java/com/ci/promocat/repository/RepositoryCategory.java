package com.ci.promocat.repository;

import com.ci.promocat.model.DAO.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryCategory extends CrudRepository<Category, Long> {

	Category findByLabel(String label);
}
