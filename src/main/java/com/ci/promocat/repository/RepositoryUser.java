package com.ci.promocat.repository;

import com.ci.promocat.model.DAO.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryUser extends CrudRepository<User, Long> {

	User findByEmail(String email);
}
