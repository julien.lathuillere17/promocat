package com.ci.promocat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import com.ci.promocat.exception.ProductNotFoundException;
import com.ci.promocat.model.DAO.Category;
import com.ci.promocat.model.DAO.Product;
import com.ci.promocat.model.DAO.Promotion;
import com.ci.promocat.model.DTO.DTOCategory;
import com.ci.promocat.model.DTO.DTOProduct;
import com.ci.promocat.model.DTO.DTOPromotion;
import com.ci.promocat.repository.RepositoryCategory;
import com.ci.promocat.repository.RepositoryProduct;
import com.ci.promocat.repository.RepositoryPromotion;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ServiceProduct extends ServiceGlobal {

	@Resource
	private RepositoryProduct repositoryProduct;

	@Resource
	private RepositoryPromotion repositoryPromotion;

	@Resource
	private RepositoryCategory repositoryCategory;

	@PostConstruct
	public void init() {

	}

	public List<DTOProduct> listProducts() {
		Iterable<Product> allProducts = repositoryProduct.findAll();
		Iterable<Promotion> allPromotions = repositoryPromotion.findAll();

		List<DTOProduct> dtoProducts = new ArrayList<>();
		for(Product product : allProducts) {
			List<Promotion> promotions = new ArrayList<>();
			for(Promotion promotion : allPromotions) {
				if (promotion.getProduct().getId() == product.getId()) {
					promotions.add(promotion);
				}
			}
			dtoProducts.add(ServiceProduct.mapProductToDTO(product, promotions));
		}
		return dtoProducts;
	}

	public static DTOProduct mapProductToDTO(Product product, List<Promotion> promotions) {
		return new DTOProduct(product.getId(), product.getLabel(), product.getDescription(), product.getImage(), product.getPrice(), ServiceCategory.mapCategoryToDTO(product.getCategory()),
			promotions.stream().map(ServiceProduct::mapPromotionToDTO).collect(Collectors.toList()));
	}

	public static DTOPromotion mapPromotionToDTO(Promotion promotion) {
		if (promotion == null) {
			return null;
		}
		return new DTOPromotion(promotion.getId(), promotion.getTaux(), promotion.getStartDate(), promotion.getEndDate());
	}

	public void addProduct(String token, DTOProduct dtoProduct) {
		findUserByToken(token);

		DTOCategory dtoCategory = dtoProduct.getCategory();

		Category category = null;
		// Si jamais la catégorie n'est pas créé on va la créé
		if (dtoCategory != null) {
			category = repositoryCategory.findByLabel(dtoCategory.getLabel());
			if (category == null) {
				category = repositoryCategory.save(new Category(dtoCategory.getLabel()));
			}
		}

		// On crée le produit
		Product product = repositoryProduct.save(new Product(dtoProduct.getLabel(), dtoProduct.getDescription(), dtoProduct.getImage(), dtoProduct.getPrice(), category));

		List<DTOPromotion> dtoPromotions = dtoProduct.getPromotions();

		// Si jamais il y'a des promotion pas créé on les crée
		if (dtoPromotions != null) {
			for(DTOPromotion dtoPromotion : dtoPromotions) {
				Optional<Promotion> promotionOptional = repositoryPromotion.findById(dtoPromotion.getId());
				if (promotionOptional.isEmpty()) {
					repositoryPromotion.save(ServiceProduct.mapDTOPromotionToDAO(dtoPromotion, product));
				}
			}
		}
	}

	public static Promotion mapDTOPromotionToDAO(DTOPromotion dtoPromotion, Product product) {
		return new Promotion(dtoPromotion.getTaux(), dtoPromotion.getStartDate(), dtoPromotion.getEndDate(), product);
	}

	public DTOPromotion addPromotion(DTOPromotion dtoPromotion, Long productId, String token) {
		findUserByToken(token);

		Optional<Product> productOptional = repositoryProduct.findById(productId);
		if (productOptional.isEmpty()) {
			throw new ProductNotFoundException("Cannot find product of id %d", productOptional);
		}
		Product product = productOptional.get();
		return ServiceProduct.mapPromotionToDTO(repositoryPromotion.save(ServiceProduct.mapDTOPromotionToDAO(dtoPromotion, product)));
	}

	public void modifyProduct(String token, DTOProduct dtoProduct) {
		findUserByToken(token);

		Optional<Product> productOptional = repositoryProduct.findById(dtoProduct.getId());
		if (productOptional.isEmpty()) {
			throw new ProductNotFoundException("Cannot find product of id %d", dtoProduct.getId());
		}
		Product product = productOptional.get();
		product.setPrice(dtoProduct.getPrice());
		product.setLabel(dtoProduct.getLabel());
		product.setImage(dtoProduct.getImage());
		product.setDescription(dtoProduct.getDescription());

		DTOCategory dtoCategory = dtoProduct.getCategory();
		Category category = repositoryCategory.findByLabel(dtoCategory.getLabel());
		if (category == null) {
			category = repositoryCategory.save(new Category(dtoCategory.getLabel()));
		}
		product.setCategory(category);

		repositoryProduct.save(product);
	}

	public void deleteProduct(String token, Long productId) {
		findUserByToken(token);

		Optional<Product> productOptional = repositoryProduct.findById(productId);
		if (productOptional.isEmpty()) {
			throw new ProductNotFoundException("Cannot find product of id %d", productId);
		}
		Product product = productOptional.get();
		Promotion promotion = repositoryPromotion.findByProduct(product);
		if (promotion != null) {
			repositoryPromotion.delete(promotion);
		}

		repositoryProduct.delete(product);
	}

	public void deletePromotion(Long promotionId, String token) {
		findUserByToken(token);
		repositoryPromotion.deleteById(promotionId);
	}
}
