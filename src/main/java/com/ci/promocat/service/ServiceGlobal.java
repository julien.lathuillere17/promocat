package com.ci.promocat.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import com.ci.promocat.exception.NotAuthorizedException;
import com.ci.promocat.model.Authentification;
import com.ci.promocat.model.DAO.User;

public class ServiceGlobal {

	private final static Integer TIME_BEFORE_CONNEXION_EXPIRE = 30;
	private static final Map<Authentification, User> mapTokens = new HashMap<>();

	protected User findUserByToken(String token) {
		User user = null;
		for(Map.Entry<Authentification, User> authentificationUserEntry : mapTokens.entrySet()) {
			if(authentificationUserEntry.getKey().getToken().equalsIgnoreCase(token)) {
				// On va vérifier que la date n'est pas dépassé
				if(Instant.now().minus(TIME_BEFORE_CONNEXION_EXPIRE, ChronoUnit.MINUTES).isAfter(authentificationUserEntry.getKey().getEndDate())) {
					throw new NotAuthorizedException("Time connexion of token %s is depassed", token);
				}
				user = authentificationUserEntry.getValue();
				break;
			}
		}
		if(user == null) {
			throw new NotAuthorizedException("Token %s is invalid", token);
		}
		return user;
	}

	protected String putUserInMapAndGetToken(User user) {
		// On supprime les précédentes authentifications de l'utilisateur dans la Map
		for(Map.Entry<Authentification, User> authentificationUserEntry : mapTokens.entrySet()) {
			if (authentificationUserEntry.getValue().getId() == user.getId()) {
				mapTokens.remove(authentificationUserEntry.getKey());
			}
		}

		String token = UUID.randomUUID().toString();
		mapTokens.put(new Authentification(Instant.now().plus(TIME_BEFORE_CONNEXION_EXPIRE, ChronoUnit.MINUTES), token), user);
		return token;
	}
}
