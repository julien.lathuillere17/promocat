package com.ci.promocat.service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.ci.promocat.exception.ParameterException;
import com.ci.promocat.exception.UserNotFoundException;
import com.ci.promocat.model.DAO.User;
import com.ci.promocat.model.DTO.DTOToken;
import com.ci.promocat.model.DTO.DTOUser;
import com.ci.promocat.repository.RepositoryUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ServiceUser extends ServiceGlobal {

	@Resource
	private RepositoryUser repositoryUser;

	@PostConstruct
	public void init() {

	}

	public DTOToken login(String email, String password) {
		if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
			throw new ParameterException("Parameter exception, email or password is not filled");
		}

		User user = repositoryUser.findByEmail(email);
		if (user == null) {
			throw new UserNotFoundException("Cannot find user of email %s", email);
		}
		if (BCrypt.verifyer().verify(password.toCharArray(), user.getPassword().toCharArray()).verified == false) {
			throw new UserNotFoundException("Wrong password of email %s", email);
		};
		return new DTOToken(putUserInMapAndGetToken(user));
	}

	public static DTOUser mapUserToDTO(User user) {
		return new DTOUser(user.getId(), user.getName(), user.getPrenom(), user.getEmail(), user.getPassword());
	}

	public DTOUser createFirstUser() {
		Iterable<User> all = repositoryUser.findAll();
		/*for(User user : all) {
			throw new RuntimeException("Already user defined");
		}*/

		User user = new User("LATHUILLERE", "Julien", "admin@promocat.fr", "promocatpasswd");
		repositoryUser.save(user);
		return mapUserToDTO(user);
	}
}
