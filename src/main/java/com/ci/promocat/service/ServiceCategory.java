package com.ci.promocat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import com.ci.promocat.exception.CategoryNotFoundException;
import com.ci.promocat.exception.ParameterException;
import com.ci.promocat.model.DAO.Category;
import com.ci.promocat.model.DTO.DTOCategory;
import com.ci.promocat.repository.RepositoryCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ServiceCategory extends ServiceGlobal {

	@Resource
	private RepositoryCategory repositoryCategory;

	@PostConstruct
	public void init() {

	}

	public DTOCategory addCategory(String token, String label) {
		findUserByToken(token);

		if (StringUtils.isEmpty(label)) {
			throw new ParameterException("Cannot add category of label empty");
		}

		Category save = repositoryCategory.save(new Category(label));
		return ServiceCategory.mapCategoryToDTO(save);
	}

	public static DTOCategory mapCategoryToDTO(Category category) {
		if (category == null) {
			return null;
		}
		return new DTOCategory(category.getId(), category.getLabel());
	}

	public void deleteCategory(String token, Long id) {
		findUserByToken(token);
		repositoryCategory.deleteById(id);
	}

	public DTOCategory modifyCategory(String token, DTOCategory dtoCategory) {
		findUserByToken(token);
		Optional<Category> optionalCategory = repositoryCategory.findById(dtoCategory.getId());
		if (optionalCategory.isEmpty()) {
			throw new CategoryNotFoundException("Category of id %d not found", dtoCategory.getId());
		}
		Category category = optionalCategory.get();
		category.setLabel(dtoCategory.getLabel());
		Category save = repositoryCategory.save(category);
		return ServiceCategory.mapCategoryToDTO(save);
	}

	public List<DTOCategory> listCategories(String token) {
		findUserByToken(token);

		List<DTOCategory> categories = new ArrayList<>();
		for(Category category : repositoryCategory.findAll()) {
			categories.add(ServiceCategory.mapCategoryToDTO(category));
		}
		return categories;
	}
}
